# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2018-12-14 23:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('play', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='info',
            options={'ordering': ('-nombre',)},
        ),
        migrations.RemoveField(
            model_name='info',
            name='id_info',
        ),
    ]
