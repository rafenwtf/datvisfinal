from django.core.files.storage import FileSystemStorage
from django.db.models import Count
from django.http import JsonResponse
from django.shortcuts import render
from django.db.models import Sum
import csv
from django.views.generic.edit import FormView
from .models import Play
import logging
import locale
from .models import Nodes, Info
from django.core import serializers
from django.http import HttpResponse
import pandas as pd

def graph(request):
	return render(request, 'graph/graph.html')


def cacafruti(request):
	return render(request, 'graph/cacafruti.html')


def load_csv(request):
	return render(request, 'graph/load_csv.html')


def graficaGraph(request):
	return render(request, 'graph/graficaGraph.html')

def burbujas(request):
	return render(request, 'graph/burbujas.html')


def influencers(request, number):
	data = list(Info.objects.values().annotate(followers=Count('seguidores')).order_by('-seguidores')[:number])
	#data = serializers.serialize('json',Info.objects.all().annotate(followers=Count('seguidores')).order_by('-seguidores')[:10],fields=('nombre', 'seguidores'))
	return data


def influencers_bars(request):
	data = influencers(request,10)
	return JsonResponse(data, safe = False)


def influencers_pie(request):
	data = influencers(request,5)
	return JsonResponse(data, safe = False)


def more_tweets(request, number):
	data = list(Info.objects.values().annotate(tweets=Count('n_tweets')).order_by('-n_tweets')[:number])
	return data


def more_tweets_bars(request):
	data = more_tweets(request,10)
	return JsonResponse(data, safe = False)


def more_tweets_pie(request):
	data = more_tweets(request,5)
	return JsonResponse(data, safe = False)


def more_retweets(request, number):
	data = list(Nodes.objects.values('retuiteado').order_by('retuiteado').annotate(count_retweets=Count('retuiteador')).order_by('-count_retweets')[:number])
	return data


def more_retweets_bars(request):
	data = more_retweets(request,10)
	return JsonResponse(data, safe = False)


def more_retweets_pie(request):
	data = more_retweets(request,5)
	return JsonResponse(data, safe = False)


def more_favs(request, number):
	data = list(Info.objects.values().annotate(favs=Count('n_favoritos')).order_by('-n_favoritos')[:number])
	return data


def more_favs_bars(request):
	data = more_favs(request,10)
	return JsonResponse(data, safe = False)


def more_favs_pie(request):
	data = more_favs(request,5)
	return JsonResponse(data, safe = False)


def info_panel(request):

	locale.setlocale(locale.LC_ALL,'')
	total_users = locale.format("%d",len(Nodes.objects.all()), grouping=True)
	total_tweets = locale.format("%d",Info.objects.aggregate(Sum('n_tweets'))['n_tweets__sum'], grouping=True)
	total_retweets = locale.format("%d",len(Info.objects.all()), grouping=True)
	total_favs = locale.format("%d",Info.objects.aggregate(Sum('n_favoritos'))['n_favoritos__sum'], grouping=True)

	return JsonResponse({'total_users': total_users, 'total_tweets': total_tweets, 'total_retweets': total_retweets, 'total_favs': total_favs}, safe=False)


def uploadToDB(request, nodos_file, info_file):

	data_nodos = pd.read_csv(nodos_file, sep=';',)
	data_info = pd.read_csv(info_file, sep=';',)

	if set(['Retuiteador', 'Retuiteado', 'Id_owner', 'ID_retweeter']).issubset(data_nodos.columns) and \
			set(['Nombre nodos (screen_ name)', 'Seguidores', 'Amigos (siguiendo)', 'Tweets emitidos', 'Veces favorito']).issubset(data_info.columns):
		Nodes.objects.all().delete()
		Info.objects.all().delete()
		objs = []
		for index, row in data_nodos.iterrows():
			objs.append(Nodes(
				retuiteador=row['Retuiteador'],
				retuiteado=row['Retuiteado'],
				id_owner=row['Id_owner'],
				id_retweeter=row['ID_retweeter'],
			))

		msg = Nodes.objects.bulk_create(objs)

		objs = []
		for index, row in data_info.iterrows():
			objs.append(Info(
				nombre=row['Nombre nodos (screen_ name)'],
				seguidores=row['Seguidores'],
				amigos=row['Amigos (siguiendo)'],
				n_tweets=row['Tweets emitidos'],
				n_favoritos=row['Veces favorito'],
			))

		msg = Info.objects.bulk_create(objs)

		return 0
	else:
		return -1



from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage

def simple_upload(request):
	if request.method == 'POST' and 'nodos' in request.FILES and "info_nodos" in request.FILES:

		fs = FileSystemStorage()

		nodos = request.FILES['nodos']
		if fs.exists(nodos):
			fs.delete(nodos)
		filename_nodos = fs.save(nodos.name, nodos)

		info_nodos = request.FILES['info_nodos']
		if fs.exists(info_nodos):
			fs.delete(info_nodos)
		filename_info = fs.save(info_nodos.name, nodos)

		uploaded_file_url = fs.url(filename_info)
		result = uploadToDB(request, filename_nodos, filename_info)

		if result == 0:
			return render(request, 'graph/graph.html', {
				'uploaded_file_url': uploaded_file_url
			})
		else:
			return render(request, 'graph/load_csv.html', {
				'uploaded_file_url': uploaded_file_url
			})


def getGroup(n):
	group = ""

	if (n < 500):
		group = 1
	elif (n < 1000):
		group = 2
	elif (n < 1500):
		group = 3
	elif (n < 2000):
		group = 4
	elif (n < 2500):
		group = 5

	return group


def mutual_follow_graph(request):
	data = {}
	data['nodes'] = []
	data['links'] = []
	dic = {}

	masRetuits = list(Nodes.objects.values('retuiteado').order_by('retuiteado').annotate(
		count_retweets=Count('retuiteador')).order_by('-count_retweets')[:300])

	for i in range(0, len(masRetuits) - 1):
		node = {}
		nodo = Info.objects.filter(nombre=masRetuits[i]['retuiteado'])
		# print(nodo.values()[0]['nombre'])
		if dic.get(nodo.values()[0]['nombre']) == None:
			dic[nodo.values()[0]['nombre']] = nodo.values()[0]['nombre']
		node['name'] = nodo.values()[0]['nombre']
		node['amigos'] = nodo.values()[0]['amigos']
		node['id'] = nodo.values()[0]['nombre']
		node['group'] = getGroup(nodo.values()[0]['amigos'])
		data['nodes'].append(node)

	links = list(Nodes.objects.all())

	for i in range(0, len(links) - 1):
		if (dic.get(links[i].retuiteador) != None and dic.get(links[i].retuiteado) != None):
			link = {}
			link['source'] = links[i].retuiteador
			link['target'] = links[i].retuiteado
			data['links'].append(link)

	# print(data)
	return JsonResponse(data, safe=False)


def seguidoresBurb(request):
	data = {}

	data['children'] = list(Info.objects.values('nombre', 'seguidores'));
	return JsonResponse(data, safe=False)