from __future__ import unicode_literals

from django.db import models
import datetime

class Play(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateTimeField()


class Nodes(models.Model):
    retuiteador = models.CharField(max_length=255, blank=False, null=False)
    retuiteado = models.CharField(max_length=255, blank=False, null=False)
    id_owner = models.IntegerField(null=False, blank=False, default=0)
    id_retweeter = models.IntegerField(null=False, blank=False, default=0)

    class Meta:
        ordering = ('-id_owner',)

    def str(self):
        return '%s (%d) retwitter %s (%d)' % (self.retuiteador, self.id_retweeter, self.retuiteado, self.id_retweeter)


class Info(models.Model):
    nombre = models.CharField(max_length=255, blank=False, null=False)
    seguidores = models.IntegerField(null=False, blank=False, default=0)
    amigos = models.IntegerField(null=False, blank=False, default=0)
    n_tweets = models.IntegerField(null=False, blank=False, default=0)
    n_favoritos = models.IntegerField(null=False, blank=False, default=0)

    class Meta:
        ordering = ('-nombre',)

    def str(self):
        return '%s' % (self.nombre)