from django.conf.urls import url


from .views import graph, info_panel, influencers_bars, influencers_pie, more_tweets_bars, more_tweets_pie, \
    more_retweets_bars, more_retweets_pie, more_favs_bars, more_favs_pie, \
    uploadToDB, cacafruti, load_csv, simple_upload, graficaGraph, burbujas, mutual_follow_graph, \
    seguidoresBurb

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', graph, name='graph'),
    url(r'^load_csv', load_csv ,name='load_csv'),
    url(r'^api/influencers_bars', influencers_bars, name='influencers_bars'),
    url(r'^api/influencers_pie', influencers_pie, name='influencers_pie'),
    url(r'^api/more_tweets_bars', more_tweets_bars, name='more_tweets_bars'),
    url(r'^api/more_tweets_pie', more_tweets_pie, name='more_tweets_pie'),
    url(r'^api/more_retweets_bars', more_retweets_bars, name='more_retweets_bars'),
    url(r'^api/more_retweets_pie', more_retweets_pie, name='more_retweets_pie'),
    url(r'^api/more_favs_bars', more_favs_bars, name='more_favs_bars'),
    url(r'^api/more_favs_pie', more_favs_pie, name='more_favs_pie'),
    url(r'^api/info_panel', info_panel, name='info_panel'),
    url(r'^api/uploadToDB', uploadToDB, name='uploadToDB'),
    url(r'^api/simple_upload', simple_upload, name='simple_upload'),
    url(r'^graficaGraph', graficaGraph, name='graficaGraph'),
    url(r'^burbujas', burbujas, name='burbujas'),
    url(r'^api/mutual_follow_graph', mutual_follow_graph, name='mutual_follow_graph'),
    url(r'^api/seguidoresBurb', seguidoresBurb, name='seguidoresBurb'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)