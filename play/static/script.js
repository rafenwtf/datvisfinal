

function pintar (data, file){
    // dimensions
    var width = 1000;
    var height = 1000;

    var margin = {
        top: 50,
        bottom: 50,
        left: 50,
        right: 50,
    };

    // create an svg to draw in
    var svg = d3.select("body")
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .append('g')
        .attr('transform', 'translate(' + margin.top + ',' + margin.left + ')');

    width = width - margin.left - margin.right;
    height = height - margin.top - margin.bottom;

    var simulation = d3.forceSimulation()
        // pull nodes together based on the links between them
        .force("link", d3.forceLink().id(function(d) {
            return d.id;
        })
        .strength(0.025))
        // push nodes apart to space them out
        .force("charge", d3.forceManyBody().strength(-400))
        // add some collision detection so they don't overlap
        .force("collide", d3.forceCollide().radius(12))
        // and draw them around the centre of the space
        .force("center", d3.forceCenter(width / 2, height / 2));

    // load the graph
    d3.json(file, function(error, graph) {
        // set the nodes
         // aqui tienes la lista de objetos en Json

        var nodes = graph.nodes;
        // links between nodes
        var links = graph.links;
        alert(file.nodes);

        // add the curved links to our graphic
        var link = svg.selectAll(".link")
            .data(links)
            .enter()
            .append("path")
            .attr("class", "link")
            .attr("stroke", function(d){
                return "#ddd";
            });

        // add the nodes to the graphic
        var node = svg.selectAll(".node")
            .data(nodes)
            .enter().append("g")

        // a circle to represent the node
        node.append("circle")
            .attr("class", "node")
            .attr("r", 8)
            .attr("fill", function(d) {
                return d.colour;
            })
            .on("mouseover", mouseOver(.1))
            .on("mouseout", mouseOut);

        // hover text for the node
        node.append("title")
            .text(function(d) {
                return d.twitter;
            });

        // add a label to each node
        node.append("text")
            .attr("dx", 12)
            .attr("dy", ".35em")
            .text(function(d) {
                return d.name;
            })
            .style("stroke", "black")
            .style("stroke-width", 0.5)
            .style("fill", function(d) {
                return d.colour;
            });

        // add the nodes to the simulation and
        // tell it what to do on each tick
        simulation
            .nodes(nodes)
            .on("tick", ticked);

        // add the links to the simulation
        simulation
            .force("link")
            .links(links);

        // on each tick, update node and link positions
        function ticked() {
            link.attr("d", positionLink);
            node.attr("transform", positionNode);
        }

        // links are drawn as curved paths between nodes
        function positionLink(d) {
            var offset = 30;

            var midpoint_x = (d.source.x + d.target.x) / 2;
            var midpoint_y = (d.source.y + d.target.y) / 2;

            var dx = (d.target.x - d.source.x);
            var dy = (d.target.y - d.source.y);

            var normalise = Math.sqrt((dx * dx) + (dy * dy));

            var offSetX = midpoint_x + offset*(dy/normalise);
            var offSetY = midpoint_y - offset*(dx/normalise);

            return "M" + d.source.x + "," + d.source.y +
                "S" + offSetX + "," + offSetY +
                " " + d.target.x + "," + d.target.y;
        }

        // move the node based on forces calculations
        function positionNode(d) {
            // keep the node within the boundaries of the svg
            if (d.x < 0) {
                d.x = 0
            };
            if (d.y < 0) {
                d.y = 0
            };
            if (d.x > width) {
                d.x = width
            };
            if (d.y > height) {
                d.y = height
            };
            return "translate(" + d.x + "," + d.y + ")";
        }

        // build a dictionary of nodes that are linked
        var linkedByIndex = {};
        links.forEach(function(d) {
            linkedByIndex[d.source.index + "," + d.target.index] = 1;
        });

        // check the dictionary to see if nodes are linked
        function isConnected(a, b) {
            return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index] || a.index == b.index;
        }

        // fade nodes on hover
        function mouseOver(opacity) {
            return function(d) {
                // check all other nodes to see if they're connected
                // to this one. if so, keep the opacity at 1, otherwise
                // fade
                node.style("stroke-opacity", function(o) {
                    thisOpacity = isConnected(d, o) ? 1 : opacity;
                    return thisOpacity;
                });
                node.style("fill-opacity", function(o){
                    thisOpacity = isConnected(d, o) ? 1 : opacity;
                    return thisOpacity;
                })
                // also style link accordingly
                link.style("stroke-opacity", function(o) {
                    return o.source === d || o.target === d ? 1 : opacity;
                });
            };
        }

        function mouseOut() {
            node.style("stroke-opacity", 1);
            node.style("fill-opacity", 1);
            link.style("stroke-opacity", 1);
            link.style("stroke", "#ddd");
        }

    });

}

function barras(){
    var margin = {top: 20, right: 40, bottom: 30, left: 20},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom,
        barWidth = Math.floor(width / 19) - 1;

    var x = d3.scale.linear()
        .range([barWidth / 2, width - barWidth / 2]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("right")
        .tickSize(-width)
        .tickFormat(function(d) { return Math.round(d / 1e6) + "M"; });

    // An SVG element with a bottom-right origin.
    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // A sliding container to hold the bars by birthyear.
    var birthyears = svg.append("g")
        .attr("class", "birthyears");

    // A label for the current year.
    var title = svg.append("text")
        .attr("class", "title")
        .attr("dy", ".71em")
        .text(2000);

    d3.csv("info_nodos.csv", function(error, data) {

      // Convert strings to numbers.
      data.forEach(function(d) {
        d.people = +d.people;
        d.year = +d.year;
        d.age = +d.age;
      });

      // Compute the extent of the data set in age and years.
      var age1 = d3.max(data, function(d) { return d.age; }),
          year0 = d3.min(data, function(d) { return d.year; }),
          year1 = d3.max(data, function(d) { return d.year; }),
          year = year1;

      // Update the scale domains.
      x.domain([year1 - age1, year1]);
      y.domain([0, d3.max(data, function(d) { return d.people; })]);

      // Produce a map from year and birthyear to [male, female].
      data = d3.nest()
          .key(function(d) { return d.year; })
          .key(function(d) { return d.year - d.age; })
          .rollup(function(v) { return v.map(function(d) { return d.people; }); })
          .map(data);

      // Add an axis to show the population values.
      svg.append("g")
          .attr("class", "y axis")
          .attr("transform", "translate(" + width + ",0)")
          .call(yAxis)
        .selectAll("g")
        .filter(function(value) { return !value; })
          .classed("zero", true);

      // Add labeled rects for each birthyear (so that no enter or exit is required).
      var birthyear = birthyears.selectAll(".birthyear")
          .data(d3.range(year0 - age1, year1 + 1, 5))
        .enter().append("g")
          .attr("class", "birthyear")
          .attr("transform", function(birthyear) { return "translate(" + x(birthyear) + ",0)"; });

      birthyear.selectAll("rect")
          .data(function(birthyear) { return data[year][birthyear] || [0, 0]; })
        .enter().append("rect")
          .attr("x", -barWidth / 2)
          .attr("width", barWidth)
          .attr("y", y)
          .attr("height", function(value) { return height - y(value); });

      // Add labels to show birthyear.
      birthyear.append("text")
          .attr("y", height - 4)
          .text(function(birthyear) { return birthyear; });

      // Add labels to show age (separate; not animated).
      svg.selectAll(".age")
          .data(d3.range(0, age1 + 1, 5))
        .enter().append("text")
          .attr("class", "age")
          .attr("x", function(age) { return x(year - age); })
          .attr("y", height + 4)
          .attr("dy", ".71em")
          .text(function(age) { return age; });

      // Allow the arrow keys to change the displayed year.
      window.focus();
      d3.select(window).on("keydown", function() {
        switch (d3.event.keyCode) {
          case 37: year = Math.max(year0, year - 10); break;
          case 39: year = Math.min(year1, year + 10); break;
        }
        update();
      });

      function update() {
        if (!(year in data)) return;
        title.text(year);

        birthyears.transition()
            .duration(750)
            .attr("transform", "translate(" + (x(year1) - x(year)) + ",0)");

        birthyear.selectAll("rect")
            .data(function(birthyear) { return data[year][birthyear] || [0, 0]; })
          .transition()
            .duration(750)
            .attr("y", y)
            .attr("height", function(value) { return height - y(value); });
      }
    });
}